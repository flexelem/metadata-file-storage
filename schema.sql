SET default_storage_engine=INNODB;

create table files (
   id binary(16) not null,
   version int not null,
   name varchar(255),
   file mediumblob,
   crc32 varchar(255),
   total_size bigint(20),
   created_at DATETIME,
   PRIMARY KEY (ID, version)
);