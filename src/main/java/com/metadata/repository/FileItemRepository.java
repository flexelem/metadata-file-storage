package com.metadata.repository;

import com.metadata.entity.FileItem;
import com.metadata.entity.FileItemId;
import com.metadata.entity.FileItemNoBlob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface FileItemRepository extends JpaRepository<FileItem, Long> {
    void deleteByIdAndVersion(UUID id, Long version);
    void deleteById(UUID id);

    FileItemNoBlob findFirstByNameOrderByVersionDesc(String name);

    FileItemNoBlob findFirstByIdOrderByVersionDesc(UUID id);
    FileItemNoBlob findByIdAndVersion(UUID id, Long version);
    List<FileItemNoBlob> findAllById(UUID id);

    FileItem getFirstByIdOrderByVersionDesc(UUID id);
    FileItem getByIdAndVersion(UUID id, Long version);
}
