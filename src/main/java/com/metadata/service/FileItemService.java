package com.metadata.service;

import com.metadata.entity.FileItem;
import com.metadata.entity.FileItemNoBlob;
import com.metadata.repository.FileItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FileItemService {

    private final FileItemRepository fileItemRepository;

    public FileItem saveFile(FileItem fileItem) {
        FileItemNoBlob existingFile = fileItemRepository.findFirstByNameOrderByVersionDesc(fileItem.getName());
        if (existingFile != null) { // update file version
            fileItem.setId(existingFile.getId());
            fileItem.setVersion(new Long(existingFile.getVersion() + 1L));
            return fileItemRepository.save(fileItem);
        }

        fileItem.setId(UUID.randomUUID());
        fileItem.setVersion(0L);
        return fileItemRepository.save(fileItem);
    }

    public FileItemNoBlob findFile(UUID id) {
        return fileItemRepository.findFirstByIdOrderByVersionDesc(id);
    }

    public FileItem getFile(UUID id) {
        return fileItemRepository.getFirstByIdOrderByVersionDesc(id);
    }

    public FileItem getFileWithVersion(UUID id, Long version) {
        return fileItemRepository.getByIdAndVersion(id, version);
    }

    public FileItemNoBlob findFileWithVersion(UUID id, Long version) {
        return fileItemRepository.findByIdAndVersion(id, version);
    }

    @Transactional
    public void deleteById(UUID id) {
        fileItemRepository.deleteById(id);
    }

    @Transactional
    public void deleteByIdAndVersion(UUID id, Long version) {
        fileItemRepository.deleteByIdAndVersion(id, version);
    }

    public List<FileItemNoBlob> listFileHistory(UUID id) {
        return fileItemRepository.findAllById(id);
    }
}
