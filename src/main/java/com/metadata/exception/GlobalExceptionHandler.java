package com.metadata.exception;

import org.hibernate.exception.JDBCConnectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    @ResponseStatus(HttpStatus.PAYLOAD_TOO_LARGE)
    public ResponseEntity<ErrorDTO> handleMaxUploadSize(HttpServletRequest request,
                                                        MaxUploadSizeExceededException ex) {
        LOGGER.error("Exception caused by {}", ex);
        return buildResponseEntity(request, ex, HttpStatus.PAYLOAD_TOO_LARGE);
    }

    @ExceptionHandler(JDBCConnectionException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorDTO> handleJDBCConnectionException(HttpServletRequest request,
                                                                  JDBCConnectionException ex) {
        LOGGER.error("Exception caused by {}", ex);
        return buildResponseEntity(request, ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(FileNotExistException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorDTO> handleFileNotFoundException(HttpServletRequest request,
                                                                FileNotExistException ex) {
        LOGGER.error("Exception caused by {}", ex);
        return buildResponseEntity(request, ex, HttpStatus.NOT_FOUND);
    }

    private ResponseEntity<ErrorDTO> buildResponseEntity(HttpServletRequest req,
                                                         Exception ex,
                                                         HttpStatus httpStatus) {
        return new ResponseEntity<>(
                ErrorDTO.builder()
                        .message(ex.getMessage())
                        .timestamp(LocalDateTime.now(ZoneOffset.UTC))
                        .status(httpStatus.value())
                        .requestPath(req.getServletPath())
                        .build(),
                httpStatus);
    }
}
