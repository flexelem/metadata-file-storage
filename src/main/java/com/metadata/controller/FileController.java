package com.metadata.controller;

import com.metadata.entity.FileItem;
import com.metadata.entity.FileItemNoBlob;
import com.metadata.entity.GetFileResponse;
import com.metadata.entity.ListFileResponse;
import com.metadata.entity.SaveFileResponse;
import com.metadata.exception.FileNotExistException;
import com.metadata.service.FileItemService;
import com.metadata.util.CommonUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/v1/files")
@RequiredArgsConstructor
@Api(value="Simple File Storage API", description="CRUD API for Simple File Storage")
public class FileController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileController.class);

    private final FileItemService fileItemService;
    private final CommonUtils commonUtils;

    /**
     * Upload a file.
     * If a file exists with same name then update its contentsm metadata and increment its version.
     * If a file with given name doesn't exist then create a new one.
     * @param file
     * @return new files metadata.
     * @throws IOException
     */
    @PostMapping("")
    @ApiOperation(value = "Upload a file with max limit of 20MB", response = SaveFileResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully uploaded file"),
    })
    public ResponseEntity<SaveFileResponse> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        FileItem fileItem = new FileItem();
        byte[] bytes = file.getBytes();
        String crc32 = commonUtils.calculateCrc32(bytes);
        fileItem.setTotalSize(bytes.length);
        fileItem.setName(file.getOriginalFilename());
        fileItem.setFile(bytes);
        fileItem.setCrc32(crc32);
        FileItem result = fileItemService.saveFile(fileItem);
        return new ResponseEntity<>(
                SaveFileResponse.builder()
                        .id(result.getId())
                        .version(result.getVersion())
                        .size(result.getTotalSize())
                        .name(result.getName())
                        .crc32(crc32)
                        .createdAt(result.getCreatedAt())
                        .build(),
                HttpStatus.OK);
    }

    /**
     * Delete file and its all versions by its id.
     * @param id of the file.
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") UUID id) {
        fileItemService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    /**
     * Delete file and its given version.
     * @param id of the file.
     * @param version of the file.
     * @return
     */
    @DeleteMapping("/{id}/version/{version}")
    public ResponseEntity<String> deleteVersion(@PathVariable("id") UUID id,
                                                @PathVariable("version") Long version) {
        fileItemService.deleteByIdAndVersion(id, version);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/list/{id}")
    @ApiOperation(value = "List metadata of all versions of a file", response = SaveFileResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully listed all versions a file"),
    })
    public ResponseEntity<ListFileResponse> list(@PathVariable("id") UUID id) {
        List<FileItemNoBlob> fileWithVersions = fileItemService.listFileHistory(id);
        return new ResponseEntity<>(
                ListFileResponse.builder()
                        .fileWithVersions(fileWithVersions)
                        .build(),
                HttpStatus.OK);
    }

    /**
     * Find and return the latest version of the file without its contents.
     * @param id of the file
     * @return latest version of the file with its properties.
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "Retrieve metadata of latest version of file", response = SaveFileResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved metadata of file"),
            @ApiResponse(code = 404, message = "There is no file exist with given id"),
            @ApiResponse(code = 400, message = "Bad request for retrieve file metadata"),
    })
    public ResponseEntity<GetFileResponse> findFile(@PathVariable("id") UUID id) {
        FileItemNoBlob fileItem = fileItemService.findFile(id);
        if (fileItem == null) {
            throw new FileNotExistException("File with id: " + id.toString() + " doesn't exist");
        }
        return new ResponseEntity<>(
                GetFileResponse.builder()
                        .id(fileItem.getId())
                        .version(fileItem.getVersion())
                        .name(fileItem.getName())
                        .crc32(fileItem.getCrc32())
                        .size(fileItem.getTotalSize())
                        .createdAt(fileItem.getCreatedAt())
                        .build(),
                HttpStatus.OK);
    }

    /**
     * Find and return the file with given version without its contents
     * @param id of the file.
     * @param version of the file
     * @return given version of the file with its properties.
     */
    @GetMapping("/{id}/version/{version}")
    @ApiOperation(value = "Retrieve metadata of requested version of file", response = SaveFileResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved metadata of file with given version"),
            @ApiResponse(code = 404, message = "There is no file exist with given id and version"),
            @ApiResponse(code = 400, message = "Bad request for retrieve file metadata with given version"),
    })
    public ResponseEntity<GetFileResponse> findFileWithVersion(@PathVariable("id") UUID id,
                                                               @PathVariable("version") Long version) {
        FileItemNoBlob fileItem = fileItemService.findFileWithVersion(id, version);
        if (fileItem == null) {
            throw new FileNotExistException("File with id: " + id.toString() + " doesn't exist");
        }
        return new ResponseEntity<>(
                GetFileResponse.builder()
                        .id(fileItem.getId())
                        .version(fileItem.getVersion())
                        .name(fileItem.getName())
                        .size(fileItem.getTotalSize())
                        .crc32(fileItem.getCrc32())
                        .createdAt(fileItem.getCreatedAt())
                        .build(),
                HttpStatus.OK);
    }

    /**
     * Download latest version of the file.
     * @param id of requested file.
     * @return file to download.
     */
    @GetMapping("/{id}/download")
    @ApiOperation(value = "Download file", response = SaveFileResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully downloaded file"),
            @ApiResponse(code = 404, message = "There is no file exist with given id"),
            @ApiResponse(code = 400, message = "Bad request for downloading file"),
    })
    public ResponseEntity<Resource> download(@PathVariable("id") UUID id) {
        FileItem fileItem = fileItemService.getFile(id);
        if (fileItem == null) {
            throw new FileNotExistException("File with id: " + id.toString() + " doesn't exist");
        }
        ByteArrayResource resource = new ByteArrayResource(fileItem.getFile());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Disposition", "filename=\"" + fileItem.getName() + "\"");
        return ResponseEntity.ok()
                .headers(httpHeaders)
                .contentLength(fileItem.getFile().length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

    /**
     * Download file with specific version.
     * @param id of requested file.
     * @return file to download.
     */
    @GetMapping("/{id}/version/{version}/download")
    @ApiOperation(value = "Download file", response = SaveFileResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully downloaded file with requested version"),
            @ApiResponse(code = 404, message = "There is no file exist with given id and version"),
            @ApiResponse(code = 400, message = "Bad request for downloading file"),
    })
    public ResponseEntity<Resource> downloadFileWithVersion(@PathVariable("id") UUID id,
                                                            @PathVariable("version") Long version) {
        FileItem fileItem = fileItemService.getFileWithVersion(id, version);
        if (fileItem == null) {
            throw new FileNotExistException("File with id: " + id.toString() + " doesn't exist");
        }
        ByteArrayResource resource = new ByteArrayResource(fileItem.getFile());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Disposition", "filename=\"" + fileItem.getName() + "\"");
        return ResponseEntity.ok()
                .headers(httpHeaders)
                .contentLength(fileItem.getFile().length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }
}
