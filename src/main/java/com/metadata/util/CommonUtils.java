package com.metadata.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.zip.CRC32;

@Component
public class CommonUtils {

    public String calculateCrc32(byte[] bytes) {
        CRC32 crc32 = new CRC32();
        crc32.update(bytes);
        return String.valueOf(crc32.getValue());
    }
}
