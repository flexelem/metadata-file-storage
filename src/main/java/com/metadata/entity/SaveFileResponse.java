package com.metadata.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SaveFileResponse {
    private UUID id;
    private Long version;
    private String name;
    private Long size;
    private String crc32;
    private Date createdAt;
}
