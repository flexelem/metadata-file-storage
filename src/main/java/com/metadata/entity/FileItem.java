package com.metadata.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.UUID;

@Entity
@IdClass(FileItemId.class)
@Table(name = "files")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class FileItem {

    @Id
    private UUID id;

    @Id
    private long version;

    @Column(name = "total_size")
    private long totalSize;

    private String name;
    private byte[] file;
    private String crc32;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;
}
