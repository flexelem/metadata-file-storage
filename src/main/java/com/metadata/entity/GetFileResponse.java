package com.metadata.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetFileResponse {
    private UUID id;
    private long version;
    private String name;
    private long size;
    private String crc32;
    private Date createdAt;
}
