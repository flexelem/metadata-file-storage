package com.metadata.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

@Data
public class FileItemId implements Serializable {
    private UUID id;
    private Long version;
}
