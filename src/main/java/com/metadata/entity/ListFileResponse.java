package com.metadata.entity;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ListFileResponse {
    private List<FileItemNoBlob> fileWithVersions;
}
