package com.metadata.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommonUtilsTest {
    private CommonUtils commonUtils;

    @Before
    public void setUp() {
        commonUtils = new CommonUtils();
    }

    @Test
    public void testCrc32() {
        String crc32 = commonUtils.calculateCrc32(new byte[]{123, 111});
        Assert.assertEquals("1344261643", crc32);
    }
}