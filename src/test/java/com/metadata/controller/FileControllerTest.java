package com.metadata.controller;

import com.metadata.entity.FileItem;
import com.metadata.entity.FileItemNoBlob;
import com.metadata.entity.GetFileResponse;
import com.metadata.entity.SaveFileResponse;
import com.metadata.exception.FileNotExistException;
import com.metadata.service.FileItemService;
import com.metadata.util.CommonUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class FileControllerTest {

    private FileController fileController;

    private UUID id;

    @Mock
    private FileItemService fileItemService;

    @Mock
    private CommonUtils commonUtils;

    @Mock
    private MockMultipartFile mockMultipartFile;

    @Mock
    private FileItemNoBlob fileItemNoBlob;

    @Mock
    private FileItem fileItem;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        id = UUID.randomUUID();
        fileController = new FileController(fileItemService, commonUtils);
    }

    @Test
    public void testUploadFile() throws IOException {
        // prepare
        byte[] bytes = {122, 100};
        when(mockMultipartFile.getOriginalFilename()).thenReturn("test.txt");
        when(mockMultipartFile.getBytes()).thenReturn(bytes);
        when(commonUtils.calculateCrc32(bytes)).thenReturn("3232");
        when(fileItemService.saveFile(any(FileItem.class)))
                .thenReturn(new FileItem(id, 0L, 2L, "test.txt", bytes, "3232", new Date()));

        // execute
        ResponseEntity<SaveFileResponse> responseEntity = fileController.uploadFile(mockMultipartFile);

        // verify
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("test.txt", responseEntity.getBody().getName());
    }

    @Test
    public void testDelete() {
        // prepare
        doNothing().when(fileItemService).deleteById(id);

        // execute
        ResponseEntity<String> deleteResponse = fileController.delete(id);

        // verify
        assertEquals(HttpStatus.OK, deleteResponse.getStatusCode());
    }

    @Test
    public void testDeleteWithId() {
        // prepare
        doNothing().when(fileItemService).deleteByIdAndVersion(id, 2L);

        // execute
        ResponseEntity<String> deleteResponse = fileController.deleteVersion(id, 2L);

        // verify
        assertEquals(HttpStatus.OK, deleteResponse.getStatusCode());
    }

    @Test
    public void testFindFile() {
        // prepaare
        Date date = new Date();
        when(fileItemService.findFile(id)).thenReturn(fileItemNoBlob);
        when(fileItemNoBlob.getId()).thenReturn(id);
        when(fileItemNoBlob.getVersion()).thenReturn(0L);
        when(fileItemNoBlob.getName()).thenReturn("test.txt");
        when(fileItemNoBlob.getTotalSize()).thenReturn(20L);
        when(fileItemNoBlob.getCreatedAt()).thenReturn(date);

        // execute
        ResponseEntity<GetFileResponse> responseEntity = fileController.findFile(id);

        // verify
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("test.txt", responseEntity.getBody().getName());
        assertEquals(id, responseEntity.getBody().getId());
        assertEquals(0L, responseEntity.getBody().getVersion());
        assertEquals(date, responseEntity.getBody().getCreatedAt());
        assertEquals(20L, responseEntity.getBody().getSize());
    }

    @Test(expected = FileNotExistException.class)
    public void testFindFileThrowsStatusException() {
        // prepaare
        when(fileItemService.findFile(id)).thenReturn(null);

        // execute
        fileController.findFile(id);
    }

    @Test
    public void testFindFileWithVersion() {
        // prepaare
        Date date = new Date();
        when(fileItemService.findFileWithVersion(id, 0L)).thenReturn(fileItemNoBlob);
        when(fileItemNoBlob.getId()).thenReturn(id);
        when(fileItemNoBlob.getVersion()).thenReturn(0L);
        when(fileItemNoBlob.getName()).thenReturn("test.txt");
        when(fileItemNoBlob.getTotalSize()).thenReturn(20L);
        when(fileItemNoBlob.getCreatedAt()).thenReturn(date);

        // execute
        ResponseEntity<GetFileResponse> responseEntity = fileController.findFileWithVersion(id, 0L);

        // verify
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("test.txt", responseEntity.getBody().getName());
        assertEquals(id, responseEntity.getBody().getId());
        assertEquals(0L, responseEntity.getBody().getVersion());
        assertEquals(date, responseEntity.getBody().getCreatedAt());
        assertEquals(20L, responseEntity.getBody().getSize());
    }

    @Test(expected = FileNotExistException.class)
    public void testFindFileWithVersionThrowsStatusException() {
        // prepaare
        when(fileItemService.findFileWithVersion(id, 0L)).thenReturn(null);

        // execute
        fileController.findFileWithVersion(id, 0L);
    }

    @Test
    public void testDownloadFile() {
        // prepare
        Date date = new Date();
        when(fileItemService.getFile(id)).thenReturn(fileItem);
        when(fileItem.getName()).thenReturn("test.txt");
        when(fileItem.getVersion()).thenReturn(2L);
        when(fileItem.getId()).thenReturn(id);
        when(fileItem.getCreatedAt()).thenReturn(date);
        when(fileItem.getFile()).thenReturn(new byte[] {123, 123});
        when(fileItem.getTotalSize()).thenReturn(20L);

        // execute
        ResponseEntity<Resource> responseEntity = fileController.download(id);

        // verify
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test(expected = FileNotExistException.class)
    public void testDownloadFileThrowsStatusException() {
        // prepaare
        when(fileItemService.getFile(id)).thenReturn(null);

        // execute
        fileController.download(id);
    }

    @Test
    public void testDownloadFileWithVersion() {
        // prepare
        Date date = new Date();
        when(fileItemService.getFileWithVersion(id, 3L)).thenReturn(fileItem);
        when(fileItem.getName()).thenReturn("test.txt");
        when(fileItem.getVersion()).thenReturn(2L);
        when(fileItem.getId()).thenReturn(id);
        when(fileItem.getCreatedAt()).thenReturn(date);
        when(fileItem.getFile()).thenReturn(new byte[] {123, 123});
        when(fileItem.getTotalSize()).thenReturn(20L);

        // execute
        ResponseEntity<Resource> responseEntity = fileController.downloadFileWithVersion(id, 3L);

        // verify
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test(expected = FileNotExistException.class)
    public void testDownloadFileWithVersionThrowsStatusException() {
        // prepaare
        when(fileItemService.getFileWithVersion(id, 0L)).thenReturn(null);

        // execute
        fileController.downloadFileWithVersion(id, 0L);
    }
}