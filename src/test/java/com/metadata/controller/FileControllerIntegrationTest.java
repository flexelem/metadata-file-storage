package com.metadata.controller;

import com.metadata.entity.GetFileResponse;
import com.metadata.entity.SaveFileResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FileControllerIntegrationTest {

    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate;

    @Before
    public void setUp() {
        restTemplate = new TestRestTemplate();
    }

    @Test
    public void testSaveFile() {
        SaveFileResponse responseBody = saveFileRequest().getBody();
        assertEquals("test.txt", responseBody.getName());
        assertNotNull(responseBody.getId());
        assertNotNull(responseBody.getVersion());
        assertNotNull(responseBody.getSize());
        assertNotNull(responseBody.getName());
        assertNotNull(responseBody.getCreatedAt());
    }

    @Test
    public void testFindFile() {
        ResponseEntity<SaveFileResponse> saveFileResponseEntity = saveFileRequest();
        ResponseEntity<GetFileResponse> getFileResponseEntity = restTemplate.exchange(
                "http://localhost:" + port + "/v1/files/" + saveFileResponseEntity.getBody().getId(),
                HttpMethod.GET,
                null,
                GetFileResponse.class);
        assertEquals(HttpStatus.OK, getFileResponseEntity.getStatusCode());
        assertEquals("test.txt", getFileResponseEntity.getBody().getName());
        assertEquals(saveFileResponseEntity.getBody().getId(), getFileResponseEntity.getBody().getId());
        assertEquals((saveFileResponseEntity.getBody().getVersion().compareTo(getFileResponseEntity.getBody().getVersion())),
                      0);
    }

    @Test
    public void testFindFileReturns404() {
        ResponseEntity<String> getFileResponseEntity = restTemplate.exchange(
                "http://localhost:" + port + "/v1/files/" + UUID.randomUUID().toString(),
                HttpMethod.GET,
                null,
                String.class);
        assertEquals(HttpStatus.NOT_FOUND, getFileResponseEntity.getStatusCode());
    }

    @Test
    public void testDownloadFile() throws IOException {
        ResponseEntity<SaveFileResponse> saveFileResponseEntity = saveFileRequest();
        ResponseEntity<Resource> downloadResponse = restTemplate.exchange(
                "http://localhost:" + port + "/v1/files/" + saveFileResponseEntity.getBody().getId() + "/download",
                HttpMethod.GET,
                null,
                Resource.class);
        assertEquals(HttpStatus.OK, downloadResponse.getStatusCode());
        byte[] downloadByteArr = FileCopyUtils.copyToByteArray(downloadResponse.getBody().getInputStream());
        byte[] expectedByteArr = FileCopyUtils.copyToByteArray(new ClassPathResource("test.txt").getInputStream());
        assertEquals(downloadByteArr.length, expectedByteArr.length);
    }

    @Test
    public void testDownloadFileReturns404() {
        ResponseEntity<String> downloadFileResponse = restTemplate.exchange(
                "http://localhost:" + port + "/v1/files/" + UUID.randomUUID().toString() + "/download",
                HttpMethod.GET,
                null,
                String.class);
        assertEquals(HttpStatus.NOT_FOUND, downloadFileResponse.getStatusCode());
    }

    @Test
    public void testDownloadFileWithVersion() throws IOException {
        ResponseEntity<SaveFileResponse> saveFileResponseEntity = saveFileRequest();
        ResponseEntity<Resource> downloadResponse = restTemplate.exchange(
                "http://localhost:" + port + "/v1/files/" + saveFileResponseEntity.getBody().getId() + "/version/" +
                    saveFileResponseEntity.getBody().getVersion() + "/download",
                HttpMethod.GET,
                null,
                Resource.class);
        assertEquals(HttpStatus.OK, downloadResponse.getStatusCode());
        byte[] downloadByteArr = FileCopyUtils.copyToByteArray(downloadResponse.getBody().getInputStream());
        byte[] expectedByteArr = FileCopyUtils.copyToByteArray(new ClassPathResource("test.txt").getInputStream());
        assertEquals(downloadByteArr.length, expectedByteArr.length);
    }

    @Test
    public void testDownloadFileWithVersionReturns404() {
        ResponseEntity<String> downloadFileResponse = restTemplate.exchange(
                "http://localhost:" + port + "/v1/files/" + UUID.randomUUID().toString() + "/version/" + 4L + "/download",
                HttpMethod.GET,
                null,
                String.class);
        assertEquals(HttpStatus.NOT_FOUND, downloadFileResponse.getStatusCode());
    }

    @Test
    public void testDeleteFile() {
        ResponseEntity<SaveFileResponse> saveResponseEntity = saveFileRequest();
        ResponseEntity<String> exchange =
            restTemplate.exchange(
                    "http://localhost:" + port + "/v1/files/" + saveResponseEntity.getBody().getId(),
                HttpMethod.DELETE,
                null,
                String.class);
        assertEquals(HttpStatus.OK, exchange.getStatusCode());
    }

    @Test
    public void testDeleteFileWithVersion() {
        ResponseEntity<SaveFileResponse> saveResponseEntity = saveFileRequest();
        ResponseEntity<String> exchange =
                restTemplate.exchange(
                        "http://localhost:" + port + "/v1/files/" + saveResponseEntity.getBody().getId() + "/version/" +
                                saveResponseEntity.getBody().getVersion(),
                        HttpMethod.DELETE,
                        null,
                        String.class);
        assertEquals(HttpStatus.OK, exchange.getStatusCode());
    }

    private ResponseEntity<SaveFileResponse> saveFileRequest() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new ClassPathResource("test.txt"));
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, httpHeaders);
        return restTemplate.exchange("http://localhost:" + port + "/v1/files/",
                                     HttpMethod.POST,
                                     requestEntity,
                                     SaveFileResponse.class);
    }
}
