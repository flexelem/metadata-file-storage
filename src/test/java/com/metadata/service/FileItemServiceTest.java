package com.metadata.service;

import com.metadata.entity.FileItem;
import com.metadata.entity.FileItemNoBlob;
import com.metadata.repository.FileItemRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class FileItemServiceTest {

    private FileItemService fileItemService;

    @Mock
    private FileItemRepository fileItemRepository;

    @Mock
    private FileItemNoBlob fileItemNoBlob;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        fileItemService = new FileItemService(fileItemRepository);
    }

    @Test
    public void testSaveFileIncrementsVersion() {
        // prepare
        FileItem fileItem = new FileItem();
        UUID uuid = UUID.randomUUID();
        fileItem.setTotalSize(123L);
        fileItem.setName("foobar.txt)");
        fileItem.setId(uuid);
        fileItem.setVersion(2L);
        when(fileItemRepository.findFirstByNameOrderByVersionDesc(fileItem.getName())).thenReturn(fileItemNoBlob);
        when(fileItemNoBlob.getVersion()).thenReturn(2L);
        when(fileItemNoBlob.getId()).thenReturn(uuid);
        when(fileItemRepository.save(fileItem)).thenReturn(fileItem);

        // execute
        FileItem result = fileItemService.saveFile(fileItem);

        // verify
        assertEquals(3L, result.getVersion());
    }

    @Test
    public void testSaveFileCreatesNewFile() {
        // prepare
        FileItem fileItem = prepareFileItem(UUID.randomUUID(), 0L, "foobar.txt");
        when(fileItemRepository.findFirstByNameOrderByVersionDesc(fileItem.getName())).thenReturn(null);
        when(fileItemRepository.save(fileItem)).thenReturn(fileItem);

        // execute
        FileItem result = fileItemService.saveFile(fileItem);

        // verify
        assertEquals(0L, result.getVersion());
        assertEquals("foobar.txt", result.getName());
        assertNotNull(result.getCreatedAt());
        assertNotNull(result.getTotalSize());
        assertNotNull(result.getFile());
    }

    @Test
    public void testFindFile() {
        // prepare
        UUID uuid = UUID.randomUUID();
        when(fileItemRepository.findFirstByIdOrderByVersionDesc(uuid)).thenReturn(fileItemNoBlob);

        // execute
        FileItemNoBlob result = fileItemService.findFile(uuid);

        // verify
        assertEquals(fileItemNoBlob, result);
    }

    @Test
    public void testGetFile() {
        // prepare
        UUID uuid = UUID.randomUUID();
        FileItem fileItem = prepareFileItem(uuid, 0L, "foobar.txt");
        when(fileItemRepository.getFirstByIdOrderByVersionDesc(uuid)).thenReturn(fileItem);

        // execute
        FileItem result = fileItemService.getFile(uuid);

        // verify
        assertEquals(fileItem, result);
        assertEquals(0L, result.getVersion());
        assertEquals(uuid, result.getId());
    }

    @Test
    public void testGetFileWithVersion() {
        // prepare
        UUID uuid = UUID.randomUUID();
        FileItem fileItem = prepareFileItem(uuid, 0L, "foobar.txt");
        when(fileItemRepository.getByIdAndVersion(uuid, 0L)).thenReturn(fileItem);

        // execute
        FileItem result = fileItemService.getFileWithVersion(uuid, 0L);

        // verify
        assertEquals(fileItem, result);
        assertEquals(0L, result.getVersion());
        assertEquals(uuid, result.getId());
    }

    @Test
    public void testFindFileWithVersion() {
        // prepare
        UUID uuid = UUID.randomUUID();
        when(fileItemRepository.findByIdAndVersion(uuid, 3L)).thenReturn(fileItemNoBlob);

        // execute
        FileItemNoBlob result = fileItemService.findFileWithVersion(uuid, 3L);

        // verify
        assertEquals(fileItemNoBlob, result);
    }

    @Test
    public void testDeleteById() {
        // prepare
        UUID uuid = UUID.randomUUID();
        doNothing().when(fileItemRepository).deleteById(uuid);

        // execute
        fileItemService.deleteById(uuid);

        // verify
        verify(fileItemRepository, times(1)).deleteById(uuid);
    }

    @Test
    public void testDeleteByIdAndVersion() {
        // prepare
        UUID uuid = UUID.randomUUID();
        doNothing().when(fileItemRepository).deleteByIdAndVersion(uuid, 2L);

        // execute
        fileItemService.deleteByIdAndVersion(uuid, 2L);

        // verify
        verify(fileItemRepository, times(1)).deleteByIdAndVersion(uuid, 2L);
    }

    private FileItem prepareFileItem(UUID id, Long version, String name) {
        FileItem fileItem = new FileItem();
        fileItem.setId(id);
        fileItem.setVersion(version);
        fileItem.setName(name);
        fileItem.setFile(new byte[] {23, 122});
        fileItem.setTotalSize(123);
        fileItem.setCreatedAt(new Date());
        return fileItem;
    }
}