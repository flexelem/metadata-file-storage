## Description of the Assignment
Implement a simple file storage with versioning support. You can upload/update/delete/get/list file(s). Every file will have its own uuid and version so that you can download either a specific version or get the latest version. Uploading a file with same name will update the contents of file and its version as well. You can either retrieve metadata information a file or download it directly.


## Tech Stack
- Spring Boot
- OSX
- MySQL v5
- JAVA 8
- Apache Maven v3.1


## Setup

You should have docker installed on your platform. Then run following commands;
- `git clone git@gitlab.com:flexelem/metadata-file-storage.git`
- `cd metadata-assignment`
- `./metadata-setup.sh`

`./metadata-setup.sh` will run docker composer which will spin-up two docker containers. One is for spring-boot web application and the other one is for mysql. The output of the script will give you your docker machine ip.

Example output of `metadata-setup.sh`
```
metadata-assignment_mysql_1 is up-to-date
metadata-assignment_web-app_1 is up-to-date
Your docker machine ip is 192.168.99.100
```

After getting docker machine ip you will be able to send requests from `http://<docker-machine-ip>:8080/v1/files`

## FEATURE ENHANCEMENTS
- Persisting files not in database but in actual file storage will be a better approach for scalability issues. 
- Adding user authentication and making each user to have their own buckets/folders.
- Providing more metadata about files like CRC-32 code to compare if downloaded file is not corrupted, file permissions, visibility etc.



## API DOC
Set of supported operations are listed below;

- POST /v1/files
- GET /v1/files/&lt;id>
- GET /v1/files/&lt;id>/version/&lt;version>
- GET /v1/files/&lt;id>/download
- GET /v1/files/&lt;id>/version/&lt;version/download
- GET /v1/files/list/&lt;id>
- DELETE /v1/files/&lt;id>
- DELETE /v1/files/&lt;id>/version/&lt;version

You can also view a simple api documentation by swagger from `http://<docker-machine-ip>:8080/swagger-ui.html`


### POST /v1/files
**Descripton** : Upload a file and return its id with metadata.

**Ex:**
```
curl -i -F file=@/Users/buraktas/Downloads/test.txt 'http://<docker-machine-ip>:8080/v1/files'
```

**Response**
```
{"id":"868b3e10-93eb-4720-aa52-076c2b32f2f1","version":1,"name":"test.txt","size":96,"crc32":"3105143452","createdAt":"2019-03-16T23:26:05.515+0000"}
```

### GET /v1/files/&lt;id>
**Descripton** : Retrieve metadata of latest version for requested file

**Parameters**

| Field        | Param Type   | Type   | Description     |
| :----------- | :----------- | :----- | :-----          |
| id           | Path Param   | UUID   | Id of the file. |


**Ex:**
```
curl -X GET 'http://<docker-machine-ip>:8080/v1/files/448cf8dd-f4ec-490e-b1f5-2dbc0e0cdf47'
```

**Response**
```
{"id":"448cf8dd-f4ec-490e-b1f5-2dbc0e0cdf47","version":1,"name":"test.txt","crc32":"3105143452","size":96,"createdAt":"2019-03-16T23:26:06.000+0000"}
```

### GET /v1/files/&lt;id>/version/&lt;version>
**Descripton** : Retrieve metadata of given version for requested file

**Parameters**

| Field        | Param Type   | Type   | Description |
| :----------- | :----------- | :----- | :----- |
| id      | Path Param   | UUID   | Id of the file. |
| version      | Path Param   | Long   | Version of the file. |

**Example Request**
```
curl -X GET 'http://<docker-machine-ip>:8080/v1/files/868b3e10-93eb-4720-aa52-076c2b32f2f1/version/0'
```

**Response**
```
{"id":"868b3e10-93eb-4720-aa52-076c2b32f2f1","version":0,"name":"test.txt","size":96,"crc32":"3105143452","createdAt":"2019-03-16T22:40:31.000+0000"}
```


### GET /v1/files/&lt;id>/download
**Descripton** : Download the latest version of file.

**Parameters**

| Field        | Param Type   | Type   | Description |
| :----------- | :----------- | :----- | :----- |
| id      | Path Param   | UUID   | Id of the file. |

**Example Request**
```
curl -X GET 'http://<machine-docker-ip>:8080/v1/files/868b3e10-93eb-4720-aa52-076c2b32f2f1/download'
```

**Response**
```
testing version 2
```


### GET /v1/files/&lt;id>/version/&lt;version>/download
**Descripton** : Download given version of file.

**Parameters**

| Field        | Param Type   | Type   | Description |
| :----------- | :----------- | :----- | :----- |
| id      | Path Param   | UUID   | Id of the file. |
| version      | Path Param   | Long   | Version of the file. |


**Example Request**
```
curl -X GET 'http://<machine-docker-ip>:8080/v1/files/868b3e10-93eb-4720-aa52-076c2b32f2f1/download'
```


**Response**
```
testing version 1
```


### GET /v1/files/list/&lt;id>
**Descripton** : List metadata of all versions of a file

**Parameters**

| Field        | Param Type   | Type   | Description |
| :----------- | :----------- | :----- | :----- |
| id      | Path Param   | UUID   | Id of the file. |


**Example Request**
```
curl -X GET 'http://<docker-machine-ip>:8080/v1/files/list/868b3e10-93eb-4720-aa52-076c2b32f2f1/'
```


**Response**
```
{"fileWithVersions":[{"version":0,"totalSize":96,"crc32":"3105143452","createdAt":"2019-03-16T22:40:31.000+0000","name":"test.txt","id":"868b3e10-93eb-4720-aa52-076c2b32f2f1"},{"version":1,"totalSize":96,"createdAt":"2019-03-16T23:26:06.000+0000","name":"test.txt","crc32":"3105143452","id":"868b3e10-93eb-4720-aa52-076c2b32f2f1"}]}
```


### DELETE /v1/files/&lt;id>
**Descripton** : Delete all versions of a file.

**Parameters**

| Field        | Param Type   | Type   | Description |
| :----------- | :----------- | :----- | :----- |
| id      | Path Param   | UUID   | Id of the file. |

**Example Request**
```
curl -X DELETE 'http://<docker-machine-ip>:8080/v1/files/448cf8dd-f4ec-490e-b1f5-2dbc0e0cdf47
```

### DELETE /v1/files/&lt;id>/version/&lt;version>
**Descripton** : Delete a version of a file.

**Parameters**

| Field        | Param Type   | Type   | Description |
| :----------- | :----------- | :----- | :----- |
| id      | Path Param   | UUID   | Id of the file. |
| version      | Path Param   | Long   | Version of the file. |

**Example Request**
```
curl -X DELETE 'http://<docker-machine-ip>:8080/v1/files/448cf8dd-f4ec-490e-b1f5-2dbc0e0cdf47/version/1'
```